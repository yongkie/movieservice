package com.demo.movieservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan({ "com.demo.movieservice" })
@EntityScan(basePackages = { "com.demo.movieservice" })
@EnableJpaRepositories({ "com.demo.movieservice" })
@ConfigurationPropertiesScan({ "com.demo.movieservice.swagger.configuration" })
public class MovieServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovieServiceApplication.class, args);
	}

}
