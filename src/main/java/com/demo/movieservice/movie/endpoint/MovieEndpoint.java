package com.demo.movieservice.movie.endpoint;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.movieservice.movie.endpoint.model.MovieModel;
import com.demo.movieservice.movie.service.MovieService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@CrossOrigin
@RequestMapping("/api/v1/movie")
public class MovieEndpoint {
	@Autowired
	MovieService movieService;

	@GetMapping("/all")
	private Flux<MovieModel> getAllMovie() {
		return movieService.findAll();
	}

	@GetMapping("/{id}")
	private Mono<MovieModel> getMovieById(@PathVariable String id) {
		return movieService.findById(id);
	}

	@PostMapping()
	public @ResponseBody MovieModel create(@RequestBody MovieModel request) throws IOException {
		return movieService.create(request);
	}

	@PutMapping()
	public @ResponseBody MovieModel update(@RequestBody MovieModel request) {
		return movieService.update(request);
	}

	@DeleteMapping("/{id}")
	public void delete(@PathVariable String id) {
		movieService.delete(id);
	}
}
