package com.demo.movieservice.movie.endpoint.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import com.demo.movieservice.base.entity.BaseEntity;
import com.demo.movieservice.comment.endpoint.model.CommentModel;
import com.demo.movieservice.image.endpoint.model.ImageModel;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MovieModel extends BaseEntity {
	private String id;
	private String title;
	private String author;
	private Double rating = 0D;
	private String description;
	private List<CommentModel> comment;
	private ImageModel image;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Double getRating() {
		return rating;
	}

	public void setRating(Double rating) {
		BigDecimal bd = new BigDecimal(rating).setScale(2, RoundingMode.FLOOR);
		this.rating = bd.doubleValue();
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ImageModel getImage() {
		return image;
	}

	public void setImage(ImageModel image) {
		this.image = image;
	}

	public List<CommentModel> getComment() {
		return comment;
	}

	public void setComment(List<CommentModel> comment) {
		this.comment = comment;
	}
}
