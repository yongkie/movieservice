package com.demo.movieservice.movie.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.movieservice.comment.endpoint.model.CommentModel;
import com.demo.movieservice.comment.entity.CommentEntity;
import com.demo.movieservice.comment.repository.CommentRepository;
import com.demo.movieservice.image.endpoint.model.ImageModel;
import com.demo.movieservice.image.entity.ImageEntity;
import com.demo.movieservice.movie.endpoint.model.MovieModel;
import com.demo.movieservice.movie.entity.MovieEntity;
import com.demo.movieservice.movie.repository.MovieRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class MovieServiceImpl implements MovieService {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	MovieRepository movieRepository;

	@Autowired
	CommentRepository commentRepository;

	@Override
	public Flux<MovieModel> findAll() {
		// TODO Auto-generated method stub
		Iterable<MovieEntity> findAllIterable = movieRepository.findAll();
		List<MovieModel> dataList = new ArrayList<MovieModel>();

		findAllIterable.forEach(data -> {
			MovieModel model = new MovieModel();
			BeanUtils.copyProperties(data, model);

			ImageModel imgModel = new ImageModel();
			BeanUtils.copyProperties(data.getImage(), imgModel);
			model.setImage(imgModel);

			try {
				Double rating = commentRepository.getRatingById(data.getId());
				if (rating == null) {
					rating = 0D;
				}
				model.setRating(rating);
			} catch (Exception e) {
				// TODO: handle exception
			}
			dataList.add(model);
		});
		return Flux.fromStream(dataList.stream());
	}

	@Override
	public Mono<MovieModel> findById(String id) {
		// TODO Auto-generated method stub
		Optional<MovieEntity> entity = movieRepository.findById(id);
		MovieModel model = new MovieModel();

		entity.ifPresent(data -> {
			logger.info("[{}]", data.getAuthor());
			BeanUtils.copyProperties(data, model);
			
			//inject image data
			ImageModel imgModel = new ImageModel();
			BeanUtils.copyProperties(data.getImage(), imgModel);
			model.setImage(imgModel);
			
			//inject comment data
			Iterable<CommentEntity> iterableComment = commentRepository.findByMovie(data);
			List<CommentModel> listComment = new ArrayList<CommentModel>();
			iterableComment.forEach(commentData -> {
				CommentModel commentModel = new CommentModel();
				BeanUtils.copyProperties(commentData, commentModel);
				listComment.add(commentModel);
			});
			model.setComment(listComment);
			
			Double rating = commentRepository.getRatingById(id);
			if (rating == null) {
				rating = 0D;
			}
			model.setRating(rating);
		});
		return Mono.fromFuture(CompletableFuture.completedFuture(model));
	}

	@Override
	public MovieModel create(MovieModel model) {
		// TODO Auto-generated method stub
		MovieEntity entity = new MovieEntity();

		BeanUtils.copyProperties(model, entity);
		entity.setCreatedBy(model.getCreatedBy());
		entity.setCreatedDate(LocalDateTime.now());

		ImageEntity imgEntity = new ImageEntity();
		BeanUtils.copyProperties(model.getImage(), imgEntity);
		entity.setImage(imgEntity);

		entity = movieRepository.save(entity);
		BeanUtils.copyProperties(entity, model);
		return model;
	}

	@Override
	public MovieModel update(MovieModel model) {
		// TODO Auto-generated method stub
		MovieEntity entity = new MovieEntity();

		BeanUtils.copyProperties(model, entity);
		entity.setUpdatedBy(model.getUpdatedBy());
		entity.setUpdatedDate(LocalDateTime.now());

		ImageEntity imgEntity = new ImageEntity();
		BeanUtils.copyProperties(model.getImage(), imgEntity);
		entity.setImage(imgEntity);

		entity = movieRepository.save(entity);
		BeanUtils.copyProperties(entity, model);
		return model;
	}

	@Override
	public void delete(String id) {
		// TODO Auto-generated method stub
		commentRepository.deleteByIdMovie(id);
		movieRepository.deleteById(id);
	}

}
