package com.demo.movieservice.movie.service;

import com.demo.movieservice.movie.endpoint.model.MovieModel;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface MovieService {
	Flux<MovieModel> findAll();

	Mono<MovieModel> findById(String id);

	MovieModel create(MovieModel model);

	MovieModel update(MovieModel model);

	void delete(String id);
}
