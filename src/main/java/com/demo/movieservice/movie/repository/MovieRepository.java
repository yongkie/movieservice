package com.demo.movieservice.movie.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.demo.movieservice.movie.entity.MovieEntity;

@Repository
public interface MovieRepository extends PagingAndSortingRepository<MovieEntity, String> {

}
