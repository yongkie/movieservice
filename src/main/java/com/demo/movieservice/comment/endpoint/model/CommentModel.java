package com.demo.movieservice.comment.endpoint.model;

import com.demo.movieservice.base.entity.BaseEntity;
import com.demo.movieservice.movie.endpoint.model.MovieModel;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CommentModel extends BaseEntity {
	private String id;
	private String comment;
	private Double rating;
	private MovieModel movie;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Double getRating() {
		return rating;
	}

	public void setRating(Double rating) {
		this.rating = rating;
	}

	public MovieModel getMovie() {
		return movie;
	}

	public void setMovie(MovieModel movie) {
		this.movie = movie;
	}
}
