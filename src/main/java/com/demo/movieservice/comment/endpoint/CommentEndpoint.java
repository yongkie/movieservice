package com.demo.movieservice.comment.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.movieservice.comment.endpoint.model.CommentModel;
import com.demo.movieservice.comment.service.CommentService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@CrossOrigin
@RequestMapping("/api/v1/comment")
public class CommentEndpoint {

	@Autowired
	CommentService commentService;
	
	@GetMapping("/all")
	private Flux<CommentModel> getAllMovie() {
		return commentService.findAll();
	}
	
	@GetMapping("/{id}")
	private Mono<CommentModel> getMovieById(@PathVariable String id){
		return commentService.findById(id);
	}

	@PostMapping()
	public @ResponseBody CommentModel create(@RequestBody CommentModel request) {
		return commentService.create(request);
	}

	@PutMapping()
	public @ResponseBody CommentModel update(@RequestBody CommentModel request) {
		return commentService.update(request);
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable String id) {
		commentService.delete(id);
	}
}
