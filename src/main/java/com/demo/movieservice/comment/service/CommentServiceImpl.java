package com.demo.movieservice.comment.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.movieservice.comment.endpoint.model.CommentModel;
import com.demo.movieservice.comment.entity.CommentEntity;
import com.demo.movieservice.comment.repository.CommentRepository;
import com.demo.movieservice.movie.endpoint.model.MovieModel;
import com.demo.movieservice.movie.entity.MovieEntity;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class CommentServiceImpl implements CommentService {
	@Autowired
	CommentRepository commentRepository;

	@Override
	public Flux<CommentModel> findAll() {
		// TODO Auto-generated method stub
		Iterable<CommentEntity> findAllIterable = commentRepository.findAll();
		List<CommentModel> dataList = new ArrayList<CommentModel>();

		findAllIterable.forEach(data -> {
			CommentModel model = new CommentModel();
			BeanUtils.copyProperties(data, model);
			MovieModel mvModel = new MovieModel();
			BeanUtils.copyProperties(data.getMovie(), mvModel);
			model.setMovie(mvModel);
			dataList.add(model);
		});
		return Flux.fromStream(dataList.stream());
	}

	@Override
	public Mono<CommentModel> findById(String id) {
		// TODO Auto-generated method stub
		Optional<CommentEntity> entity = commentRepository.findById(id);
		CommentModel model = new CommentModel();
		entity.ifPresent(data -> {
			BeanUtils.copyProperties(data, model);
			MovieModel mvModel = new MovieModel();
			BeanUtils.copyProperties(data.getMovie(), mvModel);
			model.setMovie(mvModel);
		});
		return Mono.fromFuture(CompletableFuture.completedFuture(model));
	}

	@Override
	public CommentModel create(CommentModel model) {
		// TODO Auto-generated method stub
		CommentEntity entity = new CommentEntity();
		
		BeanUtils.copyProperties(model, entity);
		entity.setCreatedBy(model.getCreatedBy());
		entity.setCreatedDate(LocalDateTime.now());
		
		MovieEntity mvEntity = new MovieEntity();
		BeanUtils.copyProperties(model.getMovie(), mvEntity);
		entity.setMovie(mvEntity);

		entity = commentRepository.save(entity);
		BeanUtils.copyProperties(entity, model);
		return model;
	}

	@Override
	public CommentModel update(CommentModel model) {
		// TODO Auto-generated method stub
		CommentEntity entity = new CommentEntity();
		
		BeanUtils.copyProperties(model, entity);
		entity.setUpdatedBy(model.getUpdatedBy());
		entity.setUpdatedDate(LocalDateTime.now());

		MovieEntity mvEntity = new MovieEntity();
		BeanUtils.copyProperties(model.getMovie(), mvEntity);
		entity.setMovie(mvEntity);
		
		entity = commentRepository.save(entity);
		BeanUtils.copyProperties(entity, model);
		return model;
	}

	@Override
	public void delete(String id) {
		// TODO Auto-generated method stub
		commentRepository.deleteById(id);
	}

}
