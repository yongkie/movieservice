package com.demo.movieservice.comment.service;

import com.demo.movieservice.comment.endpoint.model.CommentModel;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface CommentService {
	Flux<CommentModel> findAll();

	Mono<CommentModel> findById(String id);

	CommentModel create(CommentModel model);

	CommentModel update(CommentModel model);

	void delete(String id);
}
