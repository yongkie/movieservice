package com.demo.movieservice.comment.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.demo.movieservice.comment.entity.CommentEntity;
import com.demo.movieservice.movie.entity.MovieEntity;

@Repository
public interface CommentRepository extends PagingAndSortingRepository<CommentEntity, String> {

	@Query(nativeQuery = true, value = "SELECT AVG(rating) FROM comment_entity WHERE id_movie=?1 GROUP BY id_movie")
	Double getRatingById(String id);
	
	@Modifying
	@Transactional
	@Query(nativeQuery = true, value = "DELETE FROM comment_entity WHERE id_movie=?1")
	void deleteByIdMovie(String id_movie);
	
	@Transactional
	Iterable<CommentEntity> findByMovie(MovieEntity movie);
}
