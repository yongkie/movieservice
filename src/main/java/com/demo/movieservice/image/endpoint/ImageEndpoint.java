package com.demo.movieservice.image.endpoint;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.demo.movieservice.image.endpoint.model.ImageModel;
import com.demo.movieservice.image.service.ImageService;

import io.swagger.annotations.ApiParam;

@RestController
@CrossOrigin
@RequestMapping("/api/v1/image")
public class ImageEndpoint {

	@Autowired
	ImageService imageService;

	@PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public @ResponseBody ImageModel create(
			@RequestPart("file") @ApiParam(value = "File", required = true) MultipartFile file) throws IOException {
		return imageService.save(file);
	}

	@GetMapping("/{id}")
	public @ResponseBody ImageModel getImageById(@PathVariable String id) {
		return imageService.findById(id);
	}
}
