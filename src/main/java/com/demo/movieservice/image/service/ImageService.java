package com.demo.movieservice.image.service;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.demo.movieservice.image.endpoint.model.ImageModel;

public interface ImageService {
	ImageModel save(MultipartFile file) throws IOException;
	
	ImageModel findById(String id);
	
	List<ImageModel> findAll();
}
