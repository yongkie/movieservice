package com.demo.movieservice.image.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.demo.movieservice.image.endpoint.model.ImageModel;
import com.demo.movieservice.image.entity.ImageEntity;
import com.demo.movieservice.image.repository.ImageRepository;

@Service
public class ImageServiceImpl implements ImageService {

	@Autowired
	ImageRepository imageRepository;

	@Override
	public ImageModel save(MultipartFile file) throws IOException {
		// TODO Auto-generated method stub
		ImageEntity entity = new ImageEntity();
		entity.setName(StringUtils.cleanPath(file.getOriginalFilename()));
		entity.setContentType(file.getContentType());
		entity.setImageData(file.getBytes());
		entity.setSize(file.getSize());

		entity = imageRepository.save(entity);
		ImageModel model = new ImageModel();
		BeanUtils.copyProperties(entity, model);

		return model;
	}

	@Override
	public ImageModel findById(String id) {
		// TODO Auto-generated method stub
		Optional<ImageEntity> entity = imageRepository.findById(id);
		ImageModel model = new ImageModel();
		entity.ifPresent(data -> {
			BeanUtils.copyProperties(data, model);
		});
		return model;
	}

	@Override
	public List<ImageModel> findAll() {
		// TODO Auto-generated method stub
		Iterable<ImageEntity> findAllIterable = imageRepository.findAll();
		List<ImageModel> dataList = new ArrayList<ImageModel>();

		findAllIterable.forEach(data -> {
			ImageModel model = new ImageModel();
			BeanUtils.copyProperties(data, model);
			dataList.add(model);
		});
		return dataList;
	}

}
