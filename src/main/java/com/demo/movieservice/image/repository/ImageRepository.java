package com.demo.movieservice.image.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.demo.movieservice.image.entity.ImageEntity;

@Repository
public interface ImageRepository extends  PagingAndSortingRepository<ImageEntity, String>{

}
